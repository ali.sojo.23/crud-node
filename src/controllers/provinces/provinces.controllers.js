const Provinces = require("../../models/provinces.model");

exports.GET = async (req, res) => {
	const province = await Provinces.find();
	res.send({ province });
};

exports.POST = async (req, res) => {
	const newProvince = {
		Province: req.body.province,
		Country: req.body.country,
	};
	const province = await new Provinces(newProvince).save();

	province.populate("Country");

	res.send({ province });
};

exports.FIND = async (req, res) => {
	const { id } = req.params;
	const province = Provinces.findById(id);
	res.send({ province });
};

exports.findByCountry = async (req, res) => {
	const { id } = req.params;
	const province = await Provinces.find({ Country: id });

	res.send({ province });
};
