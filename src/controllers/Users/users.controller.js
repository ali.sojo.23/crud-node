const User = require("../../models/users.model");
const Info = require("../../models/info.model");
const fs = require("fs");
const path = require("path");
//
//
//GET ALL
//
//
exports.GET = async (req, res) => {
	user = await User.find().populate("Auth");
	if (user) {
		res.send({ user });
	} else {
		res.send({
			message: "Error cannot find nothing",
		});
	}
};
//
//
// CREATR NEW USER
//
//
exports.POST = async (req, res) => {
	const data = {
		Nombre: req.body.firstname,
		Apellido: req.body.lastname,
		Auth: req.params.id,
	};

	user = await new User(data).save();

	res.send({ user });
};
//
//
// GET USER OBTAINED USING AUTH ID
//
//
exports.GetByAuthId = async (req, res) => {
	const { id } = req.params;
	user = await User.findOne({ Auth: id }).populate("Auth");
	info = await Info.findById(user.informacion)
		.populate("Country")
		.populate("Province");

	user.informacion = info;

	if (user) {
		res.send({ user });
	} else {
		res.send({ message: "User not found" });
	}
};

exports.PUT = async (req, res) => {
	const { id } = req.params;

	const info = {
		Country: req.body.informacion.country,
		Province: req.body.informacion.state,
		Address: req.body.informacion.address,
		Birthdate: req.body.informacion.birthday,
		Gender: req.body.informacion.gender,
		Phone: req.body.informacion.phone,
	};

	// if (user.info) {
	// 	await Info.findByIdAndUpdate(user.informacion, info);
	// } else {
	const newInfo = await new Info(info).save();
	user = await User.findByIdAndUpdate(
		id,
		{
			informacion: newInfo._id,
		},
		{
			omitUndefined: false,
		}
	);
	// }
	res.send({ user });
};

exports.SetAvatarToUser = async (req, res) => {
	const { avatar } = req.body;
	const { id } = req.params;

	if (avatar) {
		var FileName =
			Date.now().toString() + Math.random().toString() + ".jpg";
		var ruta = "public/uploads/" + FileName;
		var archivo = __dirname + "/../../" + ruta;
		fs.writeFile(archivo, avatar, "base64", (err) => {
			if (err) {
				res.status("500").send({
					message: "server error 500",
					error: err,
				});
			}
		});

		const user = await User.findByIdAndUpdate(id, { Avatar: ruta });

		res.status("200").send({ message: " User Saved Sastifactorily" });
	}
};
