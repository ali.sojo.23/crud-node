const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const userSchema = new schema(
	{
		Nombre: {
			type: String,
			required: true,
		},
		Apellido: {
			type: String,
			required: true,
		},
		Avatar: {
			type: String,
		},
		Auth: {
			type: schema.Types.ObjectId,
			ref: "Auth",
			unique: true,
		},
		Edad: {
			type: Number,
		},
		Create_at: {
			type: Date,
			default: Date.now,
		},
		isProfessional: {
			type: Boolean,
			default: false,
		},
		informacion: {
			type: schema.Types.ObjectId,
			ref: "Info",
			unique: true,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Users", userSchema);
