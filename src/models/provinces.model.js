const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const provinceSchema = new schema(
	{
		Country: {
			type: schema.Types.ObjectId,
			ref: "Country",
			required: true,
		},
		Province: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Provinces", provinceSchema);
