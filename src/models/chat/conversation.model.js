const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const countryShema = new schema(
	{
		Users: {
			type: Array,
			required: true,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Conversations", countryShema);
