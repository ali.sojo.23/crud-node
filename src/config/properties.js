const env = require("../../.env");
module.exports = {
	PORT: process.env.PORT || env.HOST_PORT,
	DB: "mongodb://localhost:27017/prueba",
};
