var fs = require("fs");
var env = require("../../.env");

exports.transporter = {
	host: env.HOST_MAIL,
	port: env.PORT_MAIL,
	secure: env.SECURE_MAIL, // true for 465, false for other ports
	auth: {
		user: env.USER_MAIL, // generated ethereal user
		pass: env.PASSWORD_MAIL, // generated ethereal password
	},
};
